const length = 5;
computerCount = 0;
var change = "o";
var selectedR = 6,
  selectedC = 6;
var check = 6;
var player1 = "",
  player2 = "";
var win = "";
var canvasWidth = window.innerWidth - 10;
var canvasHeight = window.innerHeight - 10;
var cubeSize = canvasHeight / 8;
if (canvasWidth < canvasHeight) cubeSize = canvasWidth / 8;
var tableHeightSide = (canvasHeight - cubeSize * 5) / 2;
var tableWidthSide = (canvasWidth - cubeSize * 5) / 2;
var humanOrCopmuter = "null";
class Patrat {

  initializare(patrat = null) {
    if (patrat) {
      this.patrat = patrat;
    } else {
      this.patrat = [];
      for (let r = 0; r < length; r++) {
        this.patrat[r] = [];
        for (let c = 0; c < length; c++) {
          let valueObj = {
            value: '0',
            color: "rgb(237,201,175)"
          }
          this.patrat[r][c] = valueObj;
        }
      }
    }
  }

  afisare(xInit, y) {
    let x = xInit;
    fill("rgb(194,178,128)");
    circle(tableWidthSide + cubeSize * 2.5, tableHeightSide + cubeSize * 2.5, cubeSize * 7.2);
    for (var r = 0; r < length; r++) {
      x = xInit;
      for (var c = 0; c < length; c++) {
        stroke("black");
        strokeWeight(2);
        fill("rgb(194,178,128)");
        if (change == "o") {

          rect(tableWidthSide - cubeSize / 2, tableHeightSide + cubeSize * 2.25, cubeSize / 2);
          circle(tableWidthSide - cubeSize / 4, tableHeightSide + cubeSize * 2.5, cubeSize / 2.4);
          fill("black");
          circle(tableWidthSide - cubeSize / 4, tableHeightSide + cubeSize * 2.35, cubeSize / 20);
          fill("rgb(194,178,128)");
        } else {
          rect(tableWidthSide + cubeSize * 5, tableHeightSide + cubeSize * 2.25, cubeSize / 2);
          line(tableWidthSide + cubeSize * 5.07, tableHeightSide + cubeSize * 2.32, tableWidthSide + cubeSize * 5.43, tableHeightSide + cubeSize * 2.65);
          line(tableWidthSide + cubeSize * 5.43, tableHeightSide + cubeSize * 2.32, tableWidthSide + cubeSize * 5.07, tableHeightSide + cubeSize * 2.65);
        }
        if (this.patrat[r][c].color == "red") fill("red");
        else
          fill("rgb(194,178,128)");
        rect(x, y, cubeSize, cubeSize);
        if (this.patrat[r][c].value == "o") {

          circle(x + cubeSize / 2, y + cubeSize / 2, cubeSize / 1.4);
          fill('rgb(194,178,128)');
          circle(x + cubeSize / 2, y + cubeSize / 2, cubeSize / 1.5);
          fill('black');
          circle(x + cubeSize / 2, y + cubeSize / 4, cubeSize / 13);
          fill("rgb(194,178,128)");
        }
        if (this.patrat[r][c].value == "x") {
          strokeWeight(2);
          line(x + cubeSize / 6, y + cubeSize / 6, x + cubeSize / 1.15, y + cubeSize / 1.2);
          line(x + cubeSize / 6, y + cubeSize / 1.2, x + cubeSize / 1.15, y + cubeSize / 6);

          line(x + cubeSize / 6 - 1, y + cubeSize / 6, x + cubeSize / 1.15 + 1, y + cubeSize / 1.2);
          line(x + cubeSize / 6 - 1, y + cubeSize / 1.2, x + cubeSize / 1.15 + 1, y + cubeSize / 6);
          line(x + cubeSize / 6 - 2, y + cubeSize / 6, x + cubeSize / 1.15 + 2, y + cubeSize / 1.2);
          line(x + cubeSize / 6 - 2, y + cubeSize / 1.2, x + cubeSize / 1.15 + 2, y + cubeSize / 6);
        }
        x += cubeSize;
      }
      y += cubeSize;
    }
  }


  reset() {
    for (let r = 0; r < length; r++)
      for (let c = 0; c < length; c++) {
        this.patrat[r][c].color = "rgb(237,201,175)";
        this.patrat[r][c].value = '0'
        win = "";
      }
  }
  name1() {
    player1 = name1inp.value();
    name1bt.hide();
    name1inp.hide();
  }
  name2() {
    player2 = name2inp.value();
    name2bt.hide();
    name2inp.hide();
  }
  computer() {
    let r = 1,
      c = 1;
      
      
    while ((r != 0 && r != 4 && c != 0 && c != 4) || this.patrat[r][c].value == "o") {
      r = floor(random(0, 5));
      c = floor(random(0, 5));
    }
    if (r == 0 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
      selectedC = c;
      selectedR = r;
      if (c != 4)
        this.patrat[r][length - 1].color = "red";
      this.patrat[length - 1][c].color = "red";
      if (c != 0) this.patrat[0][0].color = "red";
    
    } else if (r == 4 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
      selectedC = c;
      selectedR = r;
      if (c != 4)
        this.patrat[4][4].color = "red";
      this.patrat[0][c].color = "red";
      if (c != 0) this.patrat[4][0].color = "red";

    } else if (c == 0 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
      selectedC = c;
      selectedR = r;
      if (r != 0)
        this.patrat[0][0].color = "red";
      this.patrat[r][4].color = "red";
      if (r != 4) this.patrat[4][0].color = "red";

    } else if (c == 4 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
      selectedC = c;
      selectedR = r;
      if (r != 0)
        this.patrat[0][4].color = "red";
      this.patrat[r][0].color = "red";
      if (r != 4) this.patrat[4][4].color = "red";

    }
    
    while ((r != 0 && r != 4 && c != 0 && c != 4) || this.patrat[r][c].color!="red") {
      r = floor(random(0, 5));
      c = floor(random(0, 5));
    }

    if (this.patrat[r][c].color == "red") {



      if (r == 0 && c != 0 && c != 4) {


        this.patrat[4][c].value = this.patrat[3][c].value;
        this.patrat[3][c].value = this.patrat[2][c].value;
        this.patrat[2][c].value = this.patrat[1][c].value;
        this.patrat[1][c].value = this.patrat[0][c].value;

      }

      if (r == 4 && c != 0 && c != 4) {
        this.patrat[0][c].value = this.patrat[1][c].value;
        this.patrat[1][c].value = this.patrat[2][c].value;
        this.patrat[2][c].value = this.patrat[3][c].value;
        this.patrat[3][c].value = this.patrat[4][c].value;
      }

      if (c == 0 && r != 0 && r != 4) {
        this.patrat[r][4].value = this.patrat[r][3].value;
        this.patrat[r][3].value = this.patrat[r][2].value;
        this.patrat[r][2].value = this.patrat[r][1].value;
        this.patrat[r][1].value = this.patrat[r][0].value;
      }

      if (c == 4 && r != 0 && r != 4) {
        this.patrat[r][0].value = this.patrat[r][1].value;
        this.patrat[r][1].value = this.patrat[r][2].value;
        this.patrat[r][2].value = this.patrat[r][3].value;
        this.patrat[r][3].value = this.patrat[r][4].value;
      }

      if (r == 0 && c == 0) {
        if (selectedR == r) {
          for (let i = selectedC; i > 0; i--)
            this.patrat[0][i].value = this.patrat[0][i - 1].value;
        }
        if (selectedC == c) {
          for (let i = selectedR; i > 0; i--)
            this.patrat[i][0].value = this.patrat[i - 1][0].value;
        }
      }

      if (r == 0 && c == 4) {
        if (selectedR == r) {
          for (let i = selectedC; i < 4; i++)
            this.patrat[0][i].value = this.patrat[0][i + 1].value;
        }
        if (selectedC == c) {
          for (let i = selectedR; i > 0; i--)
            this.patrat[i][4].value = this.patrat[i - 1][4].value;
        }
      }

      if (r == 4 && c == 4) {
        if (selectedR == r) {
          for (let i = selectedC; i < 4; i++)
            this.patrat[4][i].value = this.patrat[4][i + 1].value;
        }
        if (selectedC == c) {
          for (let i = selectedR; i < 4; i++)
            this.patrat[i][4].value = this.patrat[i + 1][4].value;
        }
      }
      if (r == 4 && c == 0) {
        if (selectedR == r) {
          for (let i = selectedC; i > 0; i--)
            this.patrat[4][i].value = this.patrat[4][i - 1].value;
        }
        if (selectedC == c) {
          for (let i = selectedR; i < 4; i++)
            this.patrat[i][0].value = this.patrat[i + 1][0].value;
        }
      }
      this.patrat[r][c].value = "x";
      change = "o";
    }
    for (let i = 0; i < length; i++)
    for (let j = 0; j < length; j++)
      if (this.patrat[i][j].color == "red") this.patrat[i][j].color = "";

  }
  clicked(mousex, mousey) {
    if (humanOrCopmuter == "computer" && change == "x") this.computer();
    else {
      for (let r = 0; r < length; r++)
        for (let c = 0; c < length; c++)

          if (mousey >= tableHeightSide + cubeSize * r && mousey <= tableHeightSide + cubeSize + cubeSize * r &&
            mousex >= tableWidthSide + cubeSize * c && mousex <= tableWidthSide + cubeSize + cubeSize * c &&
            ((c < 1 || c > length - 2) || (r == 0 || r == length - 1)))

      {
        if (this.patrat[r][c].color != "red") {
          if (check != 6)
            this.patrat[selectedR][selectedC].value = change;
          for (let i = 0; i < length; i++)
            for (let j = 0; j < length; j++)
              if (this.patrat[i][j].color == "red") this.patrat[i][j].color = "";

        }


        if (this.patrat[r][c].color == "red") {

          for (let i = 0; i < length; i++)
            for (let j = 0; j < length; j++)
              if (this.patrat[i][j].color == "red") this.patrat[i][j].color = "";

          if (r == 0 && c != 0 && c != 4) {


            this.patrat[4][c].value = this.patrat[3][c].value;
            this.patrat[3][c].value = this.patrat[2][c].value;
            this.patrat[2][c].value = this.patrat[1][c].value;
            this.patrat[1][c].value = this.patrat[0][c].value;

          }

          if (r == 4 && c != 0 && c != 4) {
            this.patrat[0][c].value = this.patrat[1][c].value;
            this.patrat[1][c].value = this.patrat[2][c].value;
            this.patrat[2][c].value = this.patrat[3][c].value;
            this.patrat[3][c].value = this.patrat[4][c].value;
          }

          if (c == 0 && r != 0 && r != 4) {
            this.patrat[r][4].value = this.patrat[r][3].value;
            this.patrat[r][3].value = this.patrat[r][2].value;
            this.patrat[r][2].value = this.patrat[r][1].value;
            this.patrat[r][1].value = this.patrat[r][0].value;
          }

          if (c == 4 && r != 0 && r != 4) {
            this.patrat[r][0].value = this.patrat[r][1].value;
            this.patrat[r][1].value = this.patrat[r][2].value;
            this.patrat[r][2].value = this.patrat[r][3].value;
            this.patrat[r][3].value = this.patrat[r][4].value;
          }

          if (r == 0 && c == 0) {
            if (selectedR == r) {
              for (let i = selectedC; i > 0; i--)
                this.patrat[0][i].value = this.patrat[0][i - 1].value;
            }
            if (selectedC == c) {
              for (let i = selectedR; i > 0; i--)
                this.patrat[i][0].value = this.patrat[i - 1][0].value;
            }
          }

          if (r == 0 && c == 4) {
            if (selectedR == r) {
              for (let i = selectedC; i < 4; i++)
                this.patrat[0][i].value = this.patrat[0][i + 1].value;
            }
            if (selectedC == c) {
              for (let i = selectedR; i > 0; i--)
                this.patrat[i][4].value = this.patrat[i - 1][4].value;
            }
          }

          if (r == 4 && c == 4) {
            if (selectedR == r) {
              for (let i = selectedC; i < 4; i++)
                this.patrat[4][i].value = this.patrat[4][i + 1].value;
            }
            if (selectedC == c) {
              for (let i = selectedR; i < 4; i++)
                this.patrat[i][4].value = this.patrat[i + 1][4].value;
            }
          }
          if (r == 4 && c == 0) {
            if (selectedR == r) {
              for (let i = selectedC; i > 0; i--)
                this.patrat[4][i].value = this.patrat[4][i - 1].value;
            }
            if (selectedC == c) {
              for (let i = selectedR; i < 4; i++)
                this.patrat[i][0].value = this.patrat[i + 1][0].value;
            }
          }
          this.patrat[r][c].value = change;
          check = 6;
          if (change == "o") change = "x";
          else change = "o";


        } else if (r == 0 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
          if (this.patrat[r][c].value == change) {
            this.patrat[r][c].value = "0";
            check = 1;
          }
          selectedC = c;
          selectedR = r;
          if (c != 4)
            this.patrat[r][length - 1].color = "red";
          this.patrat[length - 1][c].color = "red";
          if (c != 0) this.patrat[0][0].color = "red";

        } else if (r == 4 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
          if (this.patrat[r][c].value == change) {
            this.patrat[r][c].value = "0";
            check = 1;
          }
          selectedC = c;
          selectedR = r;
          if (c != 4)
            this.patrat[4][4].color = "red";
          this.patrat[0][c].color = "red";
          if (c != 0) this.patrat[4][0].color = "red";

        } else if (c == 0 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
          if (this.patrat[r][c].value == change) {
            this.patrat[r][c].value = "0";
            check = 1;
          }
          selectedC = c;
          selectedR = r;
          if (r != 0)
            this.patrat[0][0].color = "red";
          this.patrat[r][4].color = "red";
          if (r != 4) this.patrat[4][0].color = "red";

        } else if (c == 4 && (this.patrat[r][c].value == "0" || this.patrat[r][c].value == change)) {
          if (this.patrat[r][c].value == change) {
            this.patrat[r][c].value = "0";
            check = 1;
          }
          selectedC = c;
          selectedR = r;
          if (r != 0)
            this.patrat[0][4].color = "red";
          this.patrat[r][0].color = "red";
          if (r != 4) this.patrat[4][4].color = "red";

        }
      }
    }

  }
  winner() {
    let count;
    let name;
    for (let r = 0; r < length; r++) {
      count = 0;
      for (let c = 1; c < length; c++) {
        if ((this.patrat[r][c].value != this.patrat[r][c - 1].value) || this.patrat[r][c].value == "0") count++;
        name = this.patrat[r][c].value;
      }

      if (count == 0) {
        if (name == "o")
          win = player1 + " won the game ♛";
        else win = player2 + " won the game ♛";
        r = length;
      }
    }
    if (this.patrat[0][0].value != 0)
      if (this.patrat[0][0].value == this.patrat[1][1].value && (this.patrat[1][1].value == this.patrat[2][2].value) &&
        (this.patrat[2][2].value == this.patrat[3][3].value) && (this.patrat[3][3].value == this.patrat[4][4].value)) {
        if (name == "o")
          win = player1 + " won the game ♛";
        else win = player2 + " won the game ♛";
      }
    if (this.patrat[0][4].value == this.patrat[1][3].value && (this.patrat[1][3].value == this.patrat[2][2].value) && this.patrat[0][4].value != "0" &&
      (this.patrat[2][2].value == this.patrat[3][1].value) && (this.patrat[3][1].value == this.patrat[4][0].value)) {
      if (name == "o")
        win = player1 + " won the game ♛";
      else win = player2 + " won the game ♛";
    }

    for (let c = 0; c < length; c++) {
      count = 0;
      for (let r = 1; r < length; r++) {
        if ((this.patrat[r - 1][c].value != this.patrat[r][c].value) || this.patrat[r][c].value == "0") count++;
        name = this.patrat[r][c].value;
      }

      if (count == 0) {
        if (name == "o")
          win = player1 + " won the game ♛ ";
        else win = player2 + " won the game ♛";
        c = length;
      }
    }
  }
}

var patrat = new Patrat();
var resetbt;
var back;
var name1bt, name2bt;
var name1inp, name2inp;
var humanbt, computerbt;


function mouseClicked() {
  patrat.clicked(mouseX, mouseY);
}

function name1() {
  patrat.name1();
}

function name2() {
  patrat.name2();
}

function reset() {
  patrat.reset();
}

function creatNameInput() {
  name1inp = createInput("Player O");
  name1inp.position(tableWidthSide + cubeSize * 1.5, tableHeightSide - cubeSize / 2);
  name1inp.size(cubeSize * 2, 10);
  name2inp = createInput("Player X");
  name2inp.position(tableWidthSide + cubeSize * 1.5, tableHeightSide + cubeSize * 5.5);
  name2inp.size(cubeSize * 2, 10);
  name1bt = createButton("✔️");
  name1bt.position(tableWidthSide + cubeSize * 1.5 + cubeSize - 8, tableHeightSide - cubeSize / 3);
  name2bt = createButton("✔️");
  name2bt.position(tableWidthSide + cubeSize * 1.5 + cubeSize - 8, tableHeightSide + cubeSize * 5.23);
  name1bt.mousePressed(name1);
  name2bt.mousePressed(name2);
  resetbt = createButton("RESET");
  resetbt.position(canvasWidth - 60, 25);
  resetbt.size(60, 25);
  resetbt.mousePressed(reset);
  resetbt.style('background-color', "lightgrey");
}

function human() {
  humanOrCopmuter = "human";
  humanbt.hide();
  computerbt.hide();
  creatNameInput();
}

function computer() {
  humanOrCopmuter = "computer";
  humanbt.hide();
  computerbt.hide();
  creatNameInput();
}


function setup() {
  back = loadImage('back.jpg');
  createCanvas(canvasWidth, canvasHeight);
  patrat.initializare();
  humanbt = createButton("Human vs Human");
  computerbt = createButton("Human vs Computer");
  humanbt.position(canvasWidth / 2 - 50, canvasHeight / 3);
  computerbt.position(canvasWidth / 2 - 60, canvasHeight / 1.5);
  humanbt.mousePressed(human);
  computerbt.mousePressed(computer);
}

function draw() {
  patrat.winner();
  background(back);
  if (humanOrCopmuter == "null")
    text("SELECT THE GAME MODE", canvasWidth / 2 - 300, canvasHeight / 2);
  if (win == "" && humanOrCopmuter != "null")
    patrat.afisare(tableWidthSide, tableHeightSide);
  else
    text(win, canvasWidth / 2 - 250, canvasHeight / 2);
  textSize(50);


}